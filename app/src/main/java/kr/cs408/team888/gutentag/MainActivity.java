package kr.cs408.team888.gutentag;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MainActivity extends FragmentActivity
{
    int mWidthPixels, mHeightPixels;
    Button tagSearch, addSchedule, appSetting;
    DayCell[][] dayCells;
    LayoutInflater inflater;
    int touchedCellRow, touchedCellCol;
    CalendarLayout calendarLayout;
    DBManager dbManager;
    HierarchyTree Tree;
    ScheduleViewerActivity scheduleViewerActivity;
    ScheduleMakerActivity scheduleMakerActivity;
    ScheduleFinderActivity scheduleFinderActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialization();

        dbManager.make_hash_table();
        dbManager.PrintHash();
        dbManager.make_tuple_table();
        dbManager.PrintTuple();

        Tree.make_tree();




        View.OnClickListener calenderClickListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getDayCellByID(v.getId());
                scheduleViewerActivity.setDateText(calendarLayout.getMonthString(), Integer.toString(calendarLayout.dateTable[touchedCellRow][touchedCellCol]));

                scheduleViewerActivity.syncDB(calendarLayout.year, calendarLayout.month, calendarLayout.dateTable[touchedCellRow][touchedCellCol], dbManager);
                scheduleViewerActivity.show();
            }
        };

        for(int i=0; i<CalendarLayout.maxWeek; i++)
        {
            for(int j=0; j<CalendarLayout.dayOfWeek; j++)
            {
                dayCells[i][j].dayLayout.setOnClickListener(calenderClickListener);
            }
        }
    }

    private void initialization()
    {
        dbManager = new DBManager(getApplicationContext(), "schedule.db", null, 2);
        Tree = new HierarchyTree(dbManager);

        tagSearch = (Button)findViewById(R.id.btn_tag_search);
        addSchedule = (Button)findViewById(R.id.btn_add_schedule_main);
        appSetting=(Button)findViewById(R.id.btn_app_setting);

        dayCells = new DayCell[CalendarLayout.maxWeek][CalendarLayout.dayOfWeek];
        for(int i=0; i<CalendarLayout.maxWeek; i++)
        {
            for(int j=0; j<CalendarLayout.dayOfWeek; j++)
            {
                dayCells[i][j] = new DayCell();

                int resID = findViewByString("day"+i+"_"+j);
                dayCells[i][j].dayLayout = (TableLayout)findViewById(resID);
                dayCells[i][j].cellId = resID;

                resID = findViewByString("date" + i + "_" + j);
                dayCells[i][j].dayText = (TextView)findViewById(resID);

                for(int k=0; k<3; k++)
                {
                    resID=findViewByString("schedule" + i + "_" + j + "_" + k);
                    dayCells[i][j].schedules[k]=(TextView)findViewById(resID);
                }
            }
        }

        calendarLayout = new CalendarLayout((TextView)findViewById(R.id.txt_year), (TextView)findViewById(R.id.txt_month), dayCells);

        calendarLayout.syncCalendar();
        calendarLayout.syncDB(dbManager);

        inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        setDisplaySize();
        final int minPixels = (mWidthPixels < mHeightPixels)? mWidthPixels : mHeightPixels;

        scheduleViewerActivity = new ScheduleViewerActivity(inflater.inflate(R.layout.activity_daily_schedule_popup, (ViewGroup)findViewById(R.id.schedule_viewer_popup)), (int)(minPixels/1.5), (int)(minPixels/1.5));
        scheduleMakerActivity = new ScheduleMakerActivity(inflater.inflate(R.layout.activity_schedule_maker_popup, (ViewGroup)findViewById(R.id.schedule_maker_popup)),this,
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        if(arg1 == -1)
                        {
                            scheduleAccept();
                        }
                        else if(arg1 == -2)
                        {
                            popupClose(scheduleMakerActivity.scheduleMakerPopup.findViewById(R.id.btn_schedule_maker_close));
                        }
                    }

        });
        scheduleFinderActivity = new ScheduleFinderActivity(inflater.inflate(R.layout.activity_tag_search_popup, (ViewGroup)findViewById(R.id.tag_search_popup)),  mWidthPixels/3,  mHeightPixels);

    }

    public void setDisplaySize()
    {
        Display d = getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        d.getMetrics(metrics);

        mWidthPixels = metrics.widthPixels;
        mHeightPixels = metrics.heightPixels;

        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
        {
            try {
                mWidthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
                mHeightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
            } catch (Exception ignored) {
            }
        }

        if (Build.VERSION.SDK_INT >= 17)
        {
            try
            {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
                mWidthPixels = realSize.x;
                mHeightPixels = realSize.y;
            }catch (Exception ignored) {}
        }
    }

    public void popupClose(View v)
    {
        switch(v.getId())
        {
            case R.id.btn_schedule_maker_close:
                scheduleMakerActivity.dismiss();
                break;
            case R.id.btn_tagSearch_close:
                scheduleFinderActivity.dismiss();
                break;
            case R.id.btn_schedule_viewer_close:
                scheduleViewerActivity.dismiss();
                break;
            default:
                break;
        }
    }

    public void scheduleAccept()
    {
        int scheduleYear = calendarLayout.year;
        int scheduleMonth = calendarLayout.month;
        int scheduleDay = scheduleMakerActivity.getDayNumber();


        if(scheduleDay<=new GregorianCalendar(scheduleYear, scheduleMonth, 1).getActualMaximum(Calendar.DAY_OF_MONTH) && scheduleDay>=0)
        {
            scheduleMakerActivity.insertDB(dbManager, scheduleYear+"-"+(scheduleMonth+1)+"-"+scheduleDay);
        }
        else
        {
            createToast("Please, type date number within proper range", Toast.LENGTH_SHORT);
        }

        calendarLayout.syncDB(dbManager);
        scheduleViewerActivity.syncDB(scheduleYear, scheduleMonth, scheduleDay, dbManager);

        scheduleMakerActivity.dismiss();
    }

    void getDayCellByID(int id)
    {
        int i, j;

        for(i=0; i<CalendarLayout.maxWeek; i++)
        {
            for(j=0; j<CalendarLayout.dayOfWeek; j++)
            {
                if(dayCells[i][j].cellId == id)
                {
                    this.touchedCellRow=i;
                    this.touchedCellCol=j;
                    return;
                }
            }
        }
    }

    public void moveMonth(View v)
    {
        switch(v.getId())
        {
            case R.id.btn_month_later:
                this.calendarLayout.goFuture();
                break;
            case R.id.btn_month_before:
                this.calendarLayout.goPast();
                break;
            default:
                break;
        }

        this.calendarLayout.syncCalendar();
        this.calendarLayout.syncDB(dbManager);
    }

    public void deleteSchedule(View v)
    {
        int index=0;
        while(index<5 && scheduleViewerActivity.deleteButton[index] != v) index++;

        if(index>=5)
        {
            return;
        }

        dbManager.deleteID(1, DataBases.CreateDB._ID, scheduleViewerActivity.scheduleID[index]);

        this.calendarLayout.syncDB(dbManager);
        this.scheduleViewerActivity.syncDB(calendarLayout.year, calendarLayout.month, calendarLayout.dateTable[touchedCellRow][touchedCellCol], dbManager);
    }

    public void findSchedule(View v)
    {
        scheduleFinderActivity.searchWithTag(dbManager);
    }

    public void goToSchedule(View v)
    {
        int index = scheduleFinderActivity.getIndexFromId(v.getId());

        String[] date = scheduleFinderActivity.scheduleDate[index].split("-");

        int year = Integer.parseInt(date[0]);
        int month = Integer.parseInt(date[1])-1;
        int day = Integer.parseInt(date[2]);

        scheduleFinderActivity.dismiss();

        scheduleViewerActivity.setDateText(calendarLayout.monthName[month-1], Integer.toString(day));

        scheduleViewerActivity.syncDB(year, month, day, dbManager);
        scheduleViewerActivity.show();
    }

    public void mainClickListener(View v)
    {
        String dateString="";
        switch(v.getId())
        {
            case R.id.btn_tag_search:
                scheduleFinderActivity.show();
                break;

            case R.id.btn_add_schedule_viewer:
                dateString = calendarLayout.dateTable[touchedCellRow][touchedCellCol] + "";
            case R.id.btn_add_schedule_main:
                scheduleMakerActivity.setDateText(calendarLayout.getMonthString(), dateString);

                scheduleMakerActivity.show();
                break;

            case R.id.btn_app_setting:
                createToast("This is App Setting Button", Toast.LENGTH_SHORT);
                break;

            default:
                break;
        }
    }

    // Toast maker
    protected void createToast(CharSequence text, int duration)
    {
        Toast t = Toast.makeText(this, text, duration);
        t.show();
    }

    // ID finder
    protected int findViewByString(String s)
    {
        return(getResources().getIdentifier(s, "id", getPackageName()));
    }

}
//testaps
