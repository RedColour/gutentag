package kr.cs408.team888.gutentag;

/**
 * Created by MinseonKim on 2016. 11. 28..
 */

public class Node<E> {
    public E data;
    public Node<E> child;
    public Node<E> sibling;

    public Node(E data) {
        this.data = data;
        child = null;
        sibling = null;
    }

    public void SetData(E data) {
        this.data = data;
    }

    public void SetChild(Node<E> child) {
        this.child = child;
    }

    public void SetSibling(Node<E> sibling) {
        this.sibling = sibling;
    }

    E GetData() {
        return data;
    }

    Node<E> GetChild() {
        return child;
    }

    Node<E> GetSibling() {
        return sibling;
    }

}
