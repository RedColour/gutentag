package kr.cs408.team888.gutentag;
import kr.cs408.team888.gutentag.*;

/**
 * Created by MinseonKim on 2016. 11. 20..
 */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class HierarchyTree {
    DBManager dbManager;

    public HierarchyTree(DBManager dbManager)
    {
        this.dbManager = dbManager;
    }

    public Tree<String> make_test_tree(){


        Tree<String> test_tree = new Tree<String>("root",this.dbManager);

        Node<String> pn;
        Node<String> child = new Node<String>("CS409");
        pn = test_tree.GetPN();
        pn.SetChild(child);
        test_tree.ParentInsertChild("CS101");
        pn = pn.GetChild();
        test_tree.SetPN(pn);
        test_tree.ParentInsertChild("test1");
        test_tree.ParentInsertChild("test2");
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();

        pn = test_tree.GetRoot();
        result = test_tree.findPlaceInTree("CS409",pn,result,1);
        System.out.println("data : "+result.get(0));

        Node<String> test = test_tree.findChild("test1");
        //find place는 확인하지 못함.
        System.out.println("data : "+test.GetData());

        return test_tree;
    }

    public Tree<String> make_tree() {


        Tree<String> HT = new Tree<String>("root",this.dbManager);
        String[][] bunchOfTree = new String[100][3];
        bunchOfTree = dbManager.getResult();

        int i = 0;

        int[] score = new int[3];
        while (true) {
            if (bunchOfTree[i][0].equals("null")) {
                break;
            } else {
                score[0] = dbManager.getScore(bunchOfTree[i][0]);
                score[1] = dbManager.getScore(bunchOfTree[i][1]);
                score[2] = dbManager.getScore(bunchOfTree[i][2]);

                String tmp1;
                if (score[0] < score[1]) {
                    if (score[1] < score[2]) {
                        //2>1>0
                        tmp1 = bunchOfTree[i][0];
                        bunchOfTree[i][0] = bunchOfTree[i][2];
                        bunchOfTree[i][2] = tmp1;
                    } else if (score[2] > score[0]) {
                        //1>2>0
                        tmp1 = bunchOfTree[i][0];
                        bunchOfTree[i][0] = bunchOfTree[i][1];
                        bunchOfTree[i][1] = bunchOfTree[i][2];
                        bunchOfTree[i][2] = tmp1;
                    } else {
                        //1>0>2
                        tmp1 = bunchOfTree[i][0];
                        bunchOfTree[i][0] = bunchOfTree[i][1];
                        bunchOfTree[i][1] = tmp1;
                    }
                } else {
                    if (score[0] < score[2]) {
                        //2>0>1
                        tmp1 = bunchOfTree[i][0];
                        bunchOfTree[i][0] = bunchOfTree[i][2];
                        bunchOfTree[i][2] = bunchOfTree[i][1];
                        bunchOfTree[i][1] = tmp1;
                    } else if (score[2] > score[1]) {
                        //0>2>1
                        tmp1 = bunchOfTree[i][1];
                        bunchOfTree[i][1] = bunchOfTree[i][2];
                        bunchOfTree[i][2] = tmp1;
                    }
                }
            }
            i++;
        }

        i=0;

        while (true){
            if(bunchOfTree[i][0].equals("null")){
                break;
            }
            System.out.println("1: "+bunchOfTree[i][0]+" 2: "+bunchOfTree[i][1]+" 3: "+bunchOfTree[i][2]);
            i++;
        }
        i=0;
        while (true) {

            if (bunchOfTree[i][0].equals("null")) {
                break;
            } else {
                Node<String> pre;

                Node<String> firstHash = null;
                Node<String> secondHash = null;
                Node<String> thirdHash = null;

                pre = HT.GetRoot(); //root
                HT.SetPN(pre);

                firstHash = HT.findChild(bunchOfTree[i][0]);//level 1

                if (firstHash == null) {
                    HT.findPlace(bunchOfTree[i][0]);

                    firstHash = HT.GetRoot();
                    firstHash = HT.findChild(bunchOfTree[i][0]);

                    HT.SetPN(firstHash);
                    secondHash = HT.findChild(bunchOfTree[i][1]);

                    if (secondHash == null) {
                        HT.findPlace(bunchOfTree[i][1]);

                        secondHash = HT.findChild(bunchOfTree[i][1]);

                        HT.SetPN(secondHash);
                        thirdHash = HT.findChild(bunchOfTree[i][2]);
                        if (thirdHash == null) {
                            HT.findPlace(bunchOfTree[i][2]);
                        }
                    } else {
                        HT.SetPN(secondHash);
                        thirdHash = HT.findChild(bunchOfTree[i][2]);
                        if (thirdHash == null) {
                            HT.findPlace(bunchOfTree[i][2]);
                        }
                    }
                } else {
                    HT.SetPN(firstHash);
                    secondHash = HT.findChild(bunchOfTree[i][1]);

                    if (secondHash == null) {
                        HT.findPlace(bunchOfTree[i][1]);

                        secondHash = HT.findChild(bunchOfTree[i][1]);

                        HT.SetPN(secondHash);
                        thirdHash = HT.findChild(bunchOfTree[i][2]);
                        if (thirdHash == null) {
                            HT.findPlace(bunchOfTree[i][2]);
                        }
                    } else {
                        HT.SetPN(secondHash);
                        thirdHash = HT.findChild(bunchOfTree[i][2]);
                        if (thirdHash == null) {
                            HT.findPlace(bunchOfTree[i][2]);
                        }
                    }
                }

            }
            System.out.println(":");

            Node<String> a;
            a=HT.GetRoot();
            HT.printAllTree(a,1);
            i++;
        }
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
        Node<String> pn;
        pn = HT.GetRoot();
        result = HT.findPlaceInTree("#cs101",pn,result,1);
        System.out.println("\n data : "+result.get(0));
        return HT;
    }

}
