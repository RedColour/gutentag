package kr.cs408.team888.gutentag;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by SeoJunyong on 2016-11-08.
 */

public class CalendarLayout
{
    static int currentYear = (new GregorianCalendar()).get(Calendar.YEAR);
    static int currentMonth = (new GregorianCalendar()).get(Calendar.MONTH);

    static int dayOfWeek = GregorianCalendar.DAY_OF_WEEK;
    static int maxWeek = 6;

    int year;
    int month;
    int[][] dateTable;

    String[] monthName = {"JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"};

    TextView yearText, monthText;

    private DayCell[][] dayCells;

    public CalendarLayout(TextView yt, TextView mt, DayCell[][] d)
    {
        this.year = currentYear;
        this.month = currentMonth;
        this.dateTable = new int[maxWeek][dayOfWeek];

        this.yearText = yt;
        this.monthText = mt;

        this.dayCells=d;

        this.setDateTable();
    }

    public void setDateTable()
    {
        GregorianCalendar c = new GregorianCalendar(year, month, 1);

        int firstDate = c.get(Calendar.DAY_OF_WEEK);
        int dayOfMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);

        for(int i=0; i<maxWeek; i++)
        {
            for(int j=0; j<dayOfWeek; j++)
            {
                int k = i*dayOfWeek + j - (firstDate-1);

                if(k>=0 && k<dayOfMonth)
                {
                    this.dateTable[i][j] = k+1;
                }
                else
                {
                    this.dateTable[i][j]=0;
                }
            }
        }
    }

    public void syncCalendar()
    {
        for(int i=0; i<maxWeek; i++)
        {
            for(int j=0; j<dayOfWeek; j++)
            {
                if(this.dateTable[i][j] == 0)
                {
                    dayCells[i][j].dayLayout.setVisibility(View.INVISIBLE);
                }
                else
                {
                    dayCells[i][j].dayLayout.setVisibility(View.VISIBLE);
                    dayCells[i][j].dayText.setText(this.dateTable[i][j] + "");
                }
            }
        }

        monthText.setText(this.getMonthString());
        yearText.setText(this.getYearString());
    }

    public void goPast()
    {
        if(this.month <= Calendar.JANUARY)
        {
            this.year--;
            this.month = Calendar.DECEMBER;
        }
        else
        {
            this.month--;
        }

        this.setDateTable();
    }

    public void goFuture()
    {
        if(this.month >= Calendar.DECEMBER)
        {
            this.year++;
            this.month = Calendar.JANUARY;
        }
        else
        {
            this.month++;
        }

        this.setDateTable();
    }

    public String getMonthString()
    {
        return(monthName[month]);
    }

    public String getYearString()
    {
        return("" + year);
    }

    public int getRowIndex(int date)
    {
        GregorianCalendar c = new GregorianCalendar(year, month, 1);

        int firstDate = c.get(Calendar.DAY_OF_WEEK);

        return((date+firstDate-2)/dayOfWeek);
    }

    public int getColIndex(int date)
    {
        GregorianCalendar c = new GregorianCalendar(year, month, 1);

        int firstDate = c.get(Calendar.DAY_OF_WEEK);

        return((date+firstDate-2)%dayOfWeek);
    }

    public void syncDB(DBManager dbm)
    {
        SQLiteDatabase db = dbm.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from schedule;", null);

        for(DayCell[] row : dayCells)
        {
            for(DayCell element : row)
            {
                element.flush();
            }
        }

        while(cursor.moveToNext())
        {
            String[] dateInfo = cursor.getString(1).split("-");
            if(Integer.parseInt(dateInfo[0])==this.year && Integer.parseInt(dateInfo[1])==this.month+1)
            {
                String desc = cursor.getString(2);//+" "+cursor.getString(3)+" "+cursor.getString(4);
                dayCells[getRowIndex(Integer.parseInt(dateInfo[2]))][getColIndex(Integer.parseInt(dateInfo[2]))].addString(desc);
            }
        }
    }
}
