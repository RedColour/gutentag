package kr.cs408.team888.gutentag;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DBManager extends SQLiteOpenHelper{

    public DBManager(Context context, String name, CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DataBases.CreateDB._CREATE_hash);
        db.execSQL(DataBases.CreateDB._CREATE);
        db.execSQL(DataBases.CreateDB._CREATE_tuple);
        db.execSQL(DataBases.CreateDB._CREATE_kakao);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS "+DataBases.CreateDB.TABLE1);
        db.execSQL("DROP TABLE IF EXISTS "+DataBases.CreateDB.TABLE2);
        db.execSQL("DROP TABLE IF EXISTS "+DataBases.CreateDB.TABLE3);
        db.execSQL("DROP TABLE IF EXISTS "+DataBases.CreateDB.TABLE4);

        onCreate(db);
    }

    public void insert(int table, String A, String B, String C, String D, String E) {
        SQLiteDatabase db = getWritableDatabase();
        if (table ==1){
            db.execSQL("insert into schedule (date, firstHash, secondHash, thirdHash) values (\'"+A + "\', \'"+B +  "\', \'"+C +"\', \'" +D+ "\');");
        }else if (table == 2){
            int B_int = Integer.parseInt(B);
            int C_int = Integer.parseInt(C);
            int D_int = Integer.parseInt(D);
            int E_int = Integer.parseInt(E);


            db.execSQL("insert into hash (hash, hashCnt, FirstCnt, SecondCnt, ThirdCnt) values ('" + A + "', " + B_int +  ", " + C_int +", " +D_int+ ", " +E_int +");");

        }else if (table == 3){

            int D_int = Integer.parseInt(D);

            db.execSQL("insert into tuple (firstHash, secondHash, thirdHash, tupleCnt) values ('" + A + "', '" + B +  "', '" + C +"', " +D_int+ ");");


        }else if (table == 4){
            db.execSQL("insert into kakao values (" + A + ", " + B +");");
        }
        db.close();
    }

    public void update(int table, String A,String Au, String B, String Bu, int incre) {
        SQLiteDatabase db = getWritableDatabase();
        if(incre==0) {
            if (table == 1) {
                db.execSQL("update schedule set " + A + " = " + Au + " where " + B + "= '" + Bu + "';");
            } else if (table == 2) {
                db.execSQL("update hash set " + A + " = " + Au + " where " + B + "= '" + Bu + "';");
            } else if (table == 3) {
                db.execSQL("update tuple set " + A + " = " + Au + " where " + B + "= '" + Bu + "';");
            } else if (table == 4) {
                db.execSQL("update kakao set " + A + " = " + Au + " where " + B + "= '" + Bu + "';");
            }
        }else{
            if (table == 1) {
                db.execSQL("update schedule set " + A + " = " + Au + "+1 where " + B + "= '" + Bu + "';");
            } else if (table == 2) {
                db.execSQL("update hash set " + A + " = " + Au + "+1 where " + B + "= '" + Bu + "';");

            } else if (table == 3) {
                db.execSQL("update tuple set " + A + " = " + Au + "+1 where " + B + "= '" + Bu + "';");
            } else if (table == 4) {
                db.execSQL("update kakao set " + A + " = " + Au + "+1 where " + B + "= '" + Bu + "';");
            }
        }
        db.close();
    }

    public void deleteID(int table, String A, int Au)
    {
        SQLiteDatabase db = getWritableDatabase();
        if (table ==1){
            db.execSQL("delete from schedule where +"+A+" = " + Au + ";");
        }else if (table == 2){
            db.execSQL("delete from hash where +"+A+" = " + Au + ";");
        }else if (table == 3){
            db.execSQL("delete from tuple where +"+A+" = " + Au + ";");
        }else if (table == 4){
            db.execSQL("delete from kakao where +"+A+" = " + Au + ";");
        }
        db.close();
    }

    public int exist(int table, String A, String Au){
        Cursor a;
        SQLiteDatabase db = getReadableDatabase();
        int result;
        if(table ==1){
            a = db.rawQuery("select date from schedule where "+A+" = "+Au+";",null);
        }else if(table==2){
            a = db.rawQuery("select hash from hash where "+A+" = '"+Au+"';",null);
        }else if (table ==3){
            a = db.rawQuery("select tupleID from tuple where "+A+" = '"+Au+"';",null);
        }else{
            a = db.rawQuery("select * from kakao where "+A+" = "+Au+";",null);
        }
        if(a.moveToNext()){
            result = 0;
        }else{
            result = 1;
        }
        return result;
    }

    public void make_hash_table(){

        Cursor a ;

        SQLiteDatabase db = getReadableDatabase();
        a = db.rawQuery("select firstHash, secondHash, thirdHash from schedule;",null);


        db = getWritableDatabase();
        db.execSQL("delete from hash;");

        int hash_exist;
        while(a.moveToNext()) {

            for(int i=0;i<=2; i++) {

                String hash = a.getString(i);
                hash_exist = this.exist(2, "hash", hash);

                if (hash_exist == 0) {

                    this.update(2, "hashCnt", "hashCnt", "hash", hash, 1);
                    if (i == 0) {
                        this.update(2, "FirstCnt", "FirstCnt", "hash", hash, 1);
                    } else if (i == 1) {
                        this.update(2, "SecondCnt", "SecondCnt", "hash", hash, 1);
                    } else if (i == 2) {
                        this.update(2, "ThirdCnt", "ThirdCnt", "hash", hash, 1);
                    }
                } else {

                    if (i == 0) {
                        this.insert(2, hash, "1", "1", "0", "0");
                    } else if (i == 1) {
                        this.insert(2, hash, "1", "0", "1", "0");
                    } else if (i == 2) {
                        this.insert(2, hash, "1", "0", "0", "1");
                    }
                }
            }
        }
        return;
    }

    public void make_tuple_table(){
        Cursor a;
        SQLiteDatabase db = getReadableDatabase();

        a = db.rawQuery("select firstHash, secondHash, thirdHash from schedule;",null);
        db = getWritableDatabase();
        db.execSQL("Delete from tuple");
        while(a.moveToNext()) {
            String A = a.getString(0);
            String B = a.getString(1);
            String C = a.getString(2);

            Cursor b;
            db = getReadableDatabase();
            b = db.rawQuery("select tupleID from tuple where firstHash = '"+A+"' and secondHash = '" + B + "' and thirdHash = '" + C +"' ;",null);

            db = getWritableDatabase();

            if (b.moveToNext()) {
                db.execSQL("update tuple set tupleCnt = tupleCnt+1 where firstHash = '" + A + "' and secondHash = '" + B + "' and thirdHash = " + C,null);
            } else {
                this.insert(3, A, B, C, "1", "0");
            }

        }
        return;
    }

    public String[][] getResult(){
        SQLiteDatabase db = getReadableDatabase();
        String[][] result = new String[100][3];
        Cursor cursor = db.rawQuery("select hash from hash where FirstCnt>0 order by FirstCnt desc;", null);
        Cursor cursor_tuple;
        int i=0,j=0;
        while(cursor.moveToNext()) {
            String A = cursor.getString(0);

            cursor_tuple = db.rawQuery("select firstHash, secondHash, thirdHash, tupleCnt from tuple;", null);

            while(cursor_tuple.moveToNext()){

                result[i][0] = cursor_tuple.getString(0); //first hash
                result[i][1] = cursor_tuple.getString(1); //second hash
                result[i][2] = cursor_tuple.getString(2); //third hash
                i++;
            }
        }
        result[i][0] = "null";
        return result;
    }
    public int getScore(String HASH){
        SQLiteDatabase db = getReadableDatabase();
        int result=0;
        Cursor cursor = db.rawQuery("select firstCnt,secondCnt,thirdCnt from hash where hash = '"+HASH+"' order by firstCnt desc;", null);
        int total_value,F_int,S_int,T_int;
        while(cursor.moveToNext()) {
            F_int = cursor.getInt(0);
            S_int = cursor.getInt(1);
            T_int = cursor.getInt(2);
            total_value = F_int * 100 + S_int * 10 + T_int;

            result= total_value;
        }
        return result;
    }

    public ArrayList<String> PrintHash() {
        SQLiteDatabase db = getReadableDatabase();

        ArrayList<String> result = new ArrayList<String>();
        String str ="";
        Cursor cursor = db.rawQuery("select hash, hashCnt, firstCnt, secondCnt, thirdCnt from hash;", null);
        while(cursor.moveToNext()) {
            result.add(cursor.getString(0));
            str += cursor.getString(0)+" "
                    +cursor.getInt(1)+" "
                    +cursor.getInt(2)+" "
                    +cursor.getInt(3)+" "
                    +cursor.getInt(4)+"\n";
        }
        System.out.println("hash :\n"+str);
        System.out.println("---------------- hash ----------------");


        return result;
    }
    public ArrayList<String> PrintTuple() {
        SQLiteDatabase db = getReadableDatabase();

        ArrayList<String> result = new ArrayList<String>();
        String str ="";

        Cursor cursor = db.rawQuery("select firstHash, secondHash, thirdHash, tupleCnt from tuple;", null);
        while(cursor.moveToNext()) {
            str += cursor.getString(0)
                    +cursor.getString(1)
                    +cursor.getString(2)
                    +cursor.getInt(3)+"\n";
        }

        System.out.println("tuple : "+str);

        return result;
    }
}
