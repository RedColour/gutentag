package kr.cs408.team888.gutentag;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;

import java.util.ArrayList;

/**
 * Created by SeoJunyong on 2016-12-02.
 */

public class ScheduleFinderActivity
{
    View scheduleFinderPopup;
    PopupWindow scheduleFinderWindow;
    Context parentContext;

    EditText tagKeyword;
    ArrayList<String> scheduleDate = new ArrayList<String>();
    ListView scheduleResultList;
    ArrayList<String> scheduleData = new ArrayList<String>();

    public ScheduleFinderActivity(View scheduleFinderPopup, int width, int height, Context parentContext, AdapterView.OnItemClickListener listener)
    {
        this.scheduleFinderPopup = scheduleFinderPopup;
        scheduleFinderWindow = new PopupWindow(scheduleFinderPopup, width, height, true);
        this.parentContext = parentContext;

        scheduleResultList = (ListView)scheduleFinderPopup.findViewById(R.id.lview_schedule_result);
        tagKeyword = (EditText)scheduleFinderPopup.findViewById(R.id.txt_hash_keyword);

        scheduleResultList.setOnItemClickListener(listener);
    }

    public void searchWithTag(DBManager dbm)
    {
        String keyword = tagKeyword.getText().toString();

        SQLiteDatabase db = dbm.getReadableDatabase();
        Cursor cursor;

        flush();

        for(int i=2; i<=4; i++)
        {
            cursor= db.rawQuery("select * from schedule order by date asc;", null);

            while(cursor.moveToNext())
            {
                String tagInfo = cursor.getString(i);
                if(tagInfo.equals(keyword))
                {
                    this.scheduleData.add(cursor.getString(1) + "\n"+ cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4));

                    this.scheduleDate.add(cursor.getString(1));
                }
            }
        }

        scheduleResultList.setAdapter(new ArrayAdapter<String>(parentContext, android.R.layout.simple_list_item_1, scheduleData));
    }

    public void flush()
    {
        tagKeyword.setText("");
        scheduleData.clear();
        scheduleDate.clear();
    }

    public void dismiss()
    {
        flush();
        scheduleFinderWindow.dismiss();
    }

    public void show()
    {
        flush();

        scheduleFinderWindow.showAtLocation(scheduleFinderPopup, Gravity.START, 0, 0);
    }
}
