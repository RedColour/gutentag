package kr.cs408.team888.gutentag;


import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by MinseonKim on 2016. 11. 20..
 */

public class Tree<E> {
    DBManager dbManager;


    private Node<E> root;
    private Node<E> pn; // present node

    public Tree() {
        root = null;
        pn = null;
    }

    public Tree(E data,DBManager dbManager) {
        this.dbManager = dbManager;
        root = new Node<E>(data);
        pn = root;
    }

    public void SetRoot(E data) {
        root = new Node<E>(data);
        pn = root;
    }

    public void SetPN(Node<E> n) {
        pn = n;
    }

    public Node<E> GetRoot() {
        return root;
    }

    public Node<E> GetPN() {
        return pn;
    }

    public void NodeChangeData(E data) {
        pn.SetData(data);
    }

    void ParentDeleteChild(E data) {
        Node<E> tmp = null;
        Node<E> pre = null;

        tmp = pn.GetChild();
        if (tmp == null) {
            System.out.println("data가 없습니다.");
            return;
        }
        if (tmp.GetData().equals(data)) {
            pn.SetChild(tmp.GetSibling());
            return;
        }
        while (true) {
            pre = tmp;
            tmp = tmp.GetSibling();
            if (tmp == null)
                return;
            if (tmp.GetData().equals(data)) {
                pre.SetSibling(tmp.GetSibling());
                break;
            }
        }
    }
    public void ParentInsertChild(E data) {
        Node<E> tmp;

        if (pn.child == null) {
            pn.SetChild(new Node<E>(data));
            return;
        }else {
            tmp = pn.GetChild();

            while (true) {
                if (tmp.GetSibling() == null) {
                    tmp.SetSibling(new Node<E>(data));
                    break;
                }
                tmp = tmp.GetSibling();
            }
        }
    }

    public Node<E> findChild(E data){
        Node<E> tmp = null;
        Node<E> pre = null;
        Node<E> ch = null;

        tmp = pn.GetChild();
        if (tmp == null) {
            return ch;
        }
        if (tmp.GetData().equals(data)) {
            return tmp;
        }
        while (true) {
            pre = tmp;
            tmp = tmp.GetSibling();
            if (tmp == null)
                return ch;
            if (tmp.GetData().equals(data)) {
                return tmp;
            }
        }
    }

    public ArrayList<ArrayList<E>> findPlaceInTree(E data, Node<E> root, ArrayList<ArrayList<E>> result, int level){
        Node<E> tmp = null;
        Node<E> pre = null;
        E value;
        ArrayList<E> result_temp = new ArrayList<E>();

        if(root==null)
            return result;
        else
            tmp = root.GetChild();

        if(root.GetData().equals(data)) {

            result_temp.add((E)Integer.toString(level));
            while(true){
                if(tmp == null){
                    break;
                }
                value = tmp.GetData();
                result_temp.add(value);

                pre = tmp;
                tmp = tmp.GetSibling();

            }//array에 child 넣기
            result.add(result_temp);

            result = findPlaceInTree(data, root.GetSibling(), result,level);
            return result;
        }

        result = findPlaceInTree(data, root.GetSibling(),result,level);
        result = findPlaceInTree(data, root.GetChild(),result,level+1);

        return result;
    }

    public void findPlace(E data){
        Node<E> tmp = null;
        Node<E> pre = null;
        Node<E> ch = new Node<>(data);


        tmp = pn.GetChild();
        if (tmp == null) {
            pn.SetChild(ch);
            return;
        }
        int tmp_score;
        int data_score;
        data_score = dbManager.getScore((String)data);

        while (true) {

            if (tmp==null){
                pre.SetSibling(ch);
                return;
            }else {
                tmp_score = dbManager.getScore((String) tmp.data);

                if (tmp_score > data_score) {
                    pre = tmp;
                    tmp = tmp.GetSibling();
                } else {

                    pre = tmp;
                    tmp = tmp.GetSibling();

                    pre.SetSibling(ch);
                    ch.SetSibling(tmp);
                    return;
                }

            }

        }
    }

    void PrintChild() {
        Node<E> tmp = pn.GetChild();
        if (tmp == null)
            System.out.println("child가 없습니다.");
        else {
            while (true) {
                if (tmp == null)
                    break;
                System.out.println(tmp.GetData());
                tmp = tmp.GetSibling();
            }
            System.out.println("\n");
        }
    }

    public void printAllTree(Node<E> root, int level){
        Node<E> tmp = null;
        Node<E> pre = null;
        E value;
        String v ="";

        if(root==null)
            return;

        System.out.print("level "+level+": "+root.GetData()+" ");

        printAllTree(root.GetChild(),level+1);
        printAllTree(root.GetSibling(),level);


        return;
    }
}

