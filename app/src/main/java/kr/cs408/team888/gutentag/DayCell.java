package kr.cs408.team888.gutentag;

import android.widget.TableLayout;
import android.widget.TextView;

/**
 * Created by SeoJunyong on 2016-11-07.
 */

public class DayCell
{
    int cellId;
    TableLayout dayLayout;
    TextView dayText;
    TextView[] schedules = new TextView[3];

    void addString(String s)//여기엔 이제 string이 아니라 schedule class를 받아야함
    {
        int i=this.lastScheduleTextView();

        if(i<3)
        {
            schedules[i].setText(s);
        }
        else
        {
            schedules[2].setText("...");
        }
    }

    int lastScheduleTextView()
    {
        int i=0;

        while(i<3 && !schedules[i].getText().toString().equals("")) i++;

        return(i);
    }

    void flush()
    {
        for(int i=0; i<3; i++)
        {
            schedules[i].setText("");
        }
    }
}
