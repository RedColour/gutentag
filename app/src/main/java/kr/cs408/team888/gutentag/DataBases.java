package kr.cs408.team888.gutentag;

import android.provider.BaseColumns;

/**
 * Created by MinseonKim on 2016. 11. 14..
 */
public class DataBases {
    public static final class CreateDB implements BaseColumns{
        public static final String DATE = "date";
        public static final String FIRSTHash = "firstHash";
        public static final String SECONDHash = "secondHash";
        public static final String THIRDHash = "thirdHash";
        public static final String TABLE1 = "schedule";

        public static final String HASH = "hash";
        public static final String HASH_id = "hashID";
        public static final String HASH_cnt = "hashCnt";
        public static final String HASH_Fcnt = "FirstCnt";
        public static final String HASH_Scnt = "SecondCnt";
        public static final String HASH_Tcnt = "ThirdCnt";
        public static final String TABLE2= "hash";

        public static final String TUPLE_id = "tupleID";
        public static final String TUPLE_cnt = "tupleCnt";
        public static final String TABLE3="tuple";

        public static final String name = "name";
        public static final String kakao_id = "kakaoID";
        public static final String TABLE4 = "kakao";

        public static final String _CREATE =
                "create table "+TABLE1+" ("+_ID+" integer primary key autoincrement, "+DATE+" text not null, " +FIRSTHash+" text not null, "+SECONDHash+","+THIRDHash+"); ";
        public static final String _CREATE_hash =
                        "create table "+TABLE2+" ("+HASH_id+" integer primary key autoincrement, "+HASH+" text not null," + HASH_cnt+ " int, "+HASH_Fcnt+" int, "+HASH_Scnt+" int, "+HASH_Tcnt+" int); ";
        public static final String _CREATE_tuple =
                        "create table "+TABLE3+" ("+TUPLE_id+" integer primary key autoincrement, " +FIRSTHash+" text not null, "+SECONDHash+", "+THIRDHash+", "+TUPLE_cnt+" int); ";
        public static final String _CREATE_kakao =
                        "create table "+TABLE4+" ("+name+", "+kakao_id+"); ";

    }
}
