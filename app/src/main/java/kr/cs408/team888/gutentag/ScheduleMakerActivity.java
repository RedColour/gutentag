package kr.cs408.team888.gutentag;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by SeoJunyong on 2016-11-27.
 */

public class ScheduleMakerActivity
{
    AlertDialog scheduleMakerDialog;
    View scheduleMakerPopup;

    EditText[] hashInput = new EditText[3];//나중에 AutoComplete
    TextView scheduleMakerMonth;
    EditText scheduleMakerDay;

    public ScheduleMakerActivity(View scheduleMakerPopup, Context parentContext, DialogInterface.OnClickListener dialogClickListener)
    {
        this.scheduleMakerPopup = scheduleMakerPopup;

        this.scheduleMakerMonth = (TextView)scheduleMakerPopup.findViewById(R.id.txt_schedule_maker_month);
        this.scheduleMakerDay = (EditText)scheduleMakerPopup.findViewById(R.id.txt_schedule_maker_day);

        //나중에 AutoComplete
        this.hashInput[0] = (EditText)scheduleMakerPopup.findViewById(R.id.txt_schedule_hash1);
        this.hashInput[1] = (EditText)scheduleMakerPopup.findViewById(R.id.txt_schedule_hash2);
        this.hashInput[2] = (EditText)scheduleMakerPopup.findViewById(R.id.txt_schedule_hash3);

        scheduleMakerDialog = createMakerDialog(parentContext, dialogClickListener);
    }


    private AlertDialog createMakerDialog(final Context parentContext, DialogInterface.OnClickListener dialogClickListener)
    {
        AlertDialog.Builder ab = new AlertDialog.Builder(parentContext);
        ab.setTitle(R.string.app_name);
        ab.setView(scheduleMakerPopup);
        ab.setPositiveButton("Accept", dialogClickListener);

        ab.setNegativeButton("Cancel", dialogClickListener);

        return ab.create();
    }

    public void setDateText(String month, String day)
    {
        this.scheduleMakerMonth.setText(month);
        this.scheduleMakerDay.setText(day);
    }

    public void insertDB(DBManager dbm, String date)
    {
        dbm.insert(1,date, "#"+ hashInput[0].getText().toString(), "#"+ hashInput[1].getText().toString(), "#"+ hashInput[2].getText().toString(), "null");
    }

    public void flush()
    {
        this.scheduleMakerMonth.setText("");
        this.scheduleMakerDay.setText("");

        for(int i=0; i<3; i++)
        {
            this.hashInput[i].setText("");
        }
    }

    public int getDayNumber()
    {
        try
        {
            int dayNumber = Integer.parseInt(scheduleMakerDay.getText().toString());

            return(dayNumber);
        }
        catch(NumberFormatException e)
        {
            return(-1);
        }
    }

    public void show()
    {
        this.scheduleMakerDialog.show();
    }

    public void dismiss()
    {
        this.flush();
        this.scheduleMakerDialog.dismiss();
    }
}
