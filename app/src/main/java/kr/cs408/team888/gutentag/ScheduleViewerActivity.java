package kr.cs408.team888.gutentag;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;


/**
 * Created by SeoJunyong on 2016-11-27.
 */

public class ScheduleViewerActivity
{
    View scheduleViewerPopup;
    PopupWindow scheduleViewerWindow;

    TextView scheduleViewerMonth, scheduleViewerDay;
    LinearLayout[] scheduleContainer = new LinearLayout[5];
    TextView[] scheduleContent = new TextView[5];
    Button[] deleteButton = new Button[5];
    Button addScheduleFromViewer;
    int[] scheduleID = new int[5];

    public ScheduleViewerActivity(final View scheduleViewerPopup, int width, int height) {
        this.scheduleViewerPopup = scheduleViewerPopup;
        this.scheduleViewerWindow = new PopupWindow(scheduleViewerPopup, width, height, true);

        this.scheduleViewerMonth = (TextView) (scheduleViewerPopup.findViewById(R.id.txt_schedule_month));
        this.scheduleViewerDay = (TextView) (scheduleViewerPopup.findViewById(R.id.txt_schedule_day));

        for (int i = 0; i < 5; i++) {
            this.scheduleContainer[i] = (LinearLayout) scheduleViewerPopup.findViewById(scheduleViewerPopup.getResources().getIdentifier("layout_day_" + (i + 1), "id", scheduleViewerPopup.getContext().getPackageName()));
            this.scheduleContainer[i].setVisibility(View.INVISIBLE);
            this.scheduleContent[i] = (TextView) scheduleViewerPopup.findViewById(scheduleViewerPopup.getResources().getIdentifier("txt_daily_schedule_" + (i + 1), "id", scheduleViewerPopup.getContext().getPackageName()));
            this.deleteButton[i] = (Button) scheduleViewerPopup.findViewById(scheduleViewerPopup.getResources().getIdentifier("btn_schedule_delete_" + (i + 1), "id", scheduleViewerPopup.getContext().getPackageName()));

        }

        addScheduleFromViewer = (Button) scheduleViewerPopup.findViewById(R.id.btn_add_schedule_viewer);
    }

    public void setDateText(String month, String day)
    {
        this.scheduleViewerMonth.setText(month);
        this.scheduleViewerDay.setText(day);
    }

    public void syncDB(int year, int month, int day, DBManager dbm)
    {
        SQLiteDatabase db = dbm.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from schedule;", null);

        int index=0;

        flush();

        while(cursor.moveToNext() && index<5)
        {
            String[] dateInfo = cursor.getString(1).split("-");
            if(Integer.parseInt(dateInfo[0])==year && Integer.parseInt(dateInfo[1])==month+1 && Integer.parseInt(dateInfo[2])==day)
            {
                this.scheduleContainer[index].setVisibility(View.VISIBLE);
                this.scheduleContent[index].setText(cursor.getString(2)+" "+cursor.getString(3)+" "+cursor.getString(4));
                this.scheduleID[index] = cursor.getInt(0);

                index++;
            }
        }
    }

    public void flush()
    {
        for(int i=0; i<5; i++)
        {
            this.scheduleContainer[i].setVisibility(View.INVISIBLE);
            this.scheduleContent[i].setText("");
        }
    }

    public void show()
    {
        scheduleViewerWindow.showAtLocation(scheduleViewerPopup, Gravity.CENTER, 0, 0);
    }

    public void dismiss()
    {
        flush();
        scheduleViewerWindow.dismiss();
    }
}
